#### Hướng dẫn cài đặt GIT

Mình sẽ hướng dẫn cài GIT (bao gồm git bash, git client for win...) và SmartGIT (thằng này là tool GUI cho GIT khá dễ dùng.)

Nói thêm là vì sao cần dùng git thì mình k đề cập ở đây. Cụ thể hướng dẫn sử dụng mình sẽ chỉ sau, tài liệu này chỉ hướng dẫn cài đặt và cấu hình cơ bản. :)

##### Bạn cần tải những gì?

+ [Git client](http://www.git-scm.com/download/win) tải về rồi cài đặt.
    * Lưu ý một số bước:
    * Chỉ dùng Git bash ![alt text](http://a.pomf.se/koarnl.png "Git bash")
    * Checkout ![alt text](http://a.pomf.se/nfkzcy.png)
+ [SmartGIT](http://www.syntevo.com/smartgit/)
    * Cài thằng này thì k có j đặc biệt, next tới finish thôi.
    * Config đây
    * License ![alt text](http://a.pomf.se/twnhkz.png)
    * Ssh ![alt text](http://a.pomf.se/rfanbd.png)
    * Mail ![alt text](http://a.pomf.se/redins.png)
    * Host provider ![alt text](http://a.pomf.se/dbtito.png) -- phần này chưa cần conf lúc này
    * Các bước còn lại để mặc định, bỏ qua.

##### Cung cấp ssh key cho mềnh - để add vào proj

+ Bật _git bash_ ![alt text](http://a.pomf.se/tvlowx.png)
+ Gõ lệnh ssh-keygen.exe -t rsa
+ Làm theo hình ![alt text](http://a.pomf.se/wbqvsz.png)
+ Chú ý bước chọn đường dẫn cứ để mặc định, bước nhập passphrase nhập đi (nó k hiện j lên đâu, đặc trưng Unix/Linux nó thế!), pass này > 4 ký tự, ráng nhớ nó nhé, sau này dùng nhiều.
+ Xong thì sẽ có _kết quả_ ![alt text](http://a.pomf.se/xkjdsj.png)
+ Xong rồi mọi người sẽ có ssh key của riêng máy mình, dùng nó để add vào tài khoản gitlab nhé. :D